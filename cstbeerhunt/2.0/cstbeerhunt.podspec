Pod::Spec.new do |s|
  s.name             = 'cstbeerhunt'
  s.version          = '2.0'
  s.summary          = 'BeerHunt'
  s.description      = 'BeerHunt.'

  s.homepage         = 'https://bitbucket.org/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'CST' => 'mobile@cst-dev.com' }
  s.source           = { :git => 'https://cristiolteanu@bitbucket.org/cstTreasurehunt/cstbeerhunt.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'

  s.source_files = 'CSTBeerHunt/Classes/**/*'

  s.resource_bundles = {
    'cstbeerhunt' => ['CSTBeerHunt/Assets/**/*.{xib,png,jpeg,jpg,storyboard,otf,ttf,xcassets}']
  }
end
